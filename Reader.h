#ifndef __READER_H__
#define __READER_H__

#include <string>
#include <ostream>
#include <cassert>

struct Reader {
	std::string version;
	std::string comment;
	enum {
		ASCII,
		BINARY,
		BINGZ,
		BINRGZ,
	} dataType;
	enum {
		STRUCTURED_POINTS,
		STRUCTURED_GRID,
		UNSTRUCTURED_GRID,
		POLYDATA,
		RECTILINEAR_GRID,
		FIELD
	} dataSet;
	Reader(const char *s);

	friend std::ostream &operator<<(std::ostream &o, const Reader &rd);
private:
	std::string dataTypeString() const {
		switch (dataType) {
			case ASCII : 
				return "ascii";
			case BINARY :
				return "binary";
			case BINGZ :
				return "binary gzip-compressed";
			case BINRGZ : 
				return "binary gzip-compressed reordered";
			default : 
				assert(!"Bad dataType");
				return "bad datatype";
		}
	}
	std::string dataSetString() const {
		switch (dataSet) {
			case STRUCTURED_POINTS :
				return "uniform rectangular";
			case STRUCTURED_GRID :
				return "curvilinear";
			case RECTILINEAR_GRID :
				return "rectilinear";
			case UNSTRUCTURED_GRID :
				return "unstuctured";
			case POLYDATA :
				return "polydata";
			case FIELD :
				return "field (generic data)";
			default :
				assert(!"Bad dataSet");
				return "bad dataSet";
		}
	}
};

#endif
