#ifndef __WRITER_H__
#define __WRITER_H__

#include "Subset.h"
#include "Reader.h"

#include <fstream>

struct Writer {
	bool tofloat, group, zip;
	int bits, compression;
	Subset ss[3];

	std::ofstream f;
	Writer(const char *output, bool tofloat, int bits, bool group, bool zip, int compression, Subset ss[3]);
	void process(const Reader &data);
};

#endif
