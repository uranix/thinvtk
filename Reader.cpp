#include "Reader.h"
#include <fstream>
#include <cstdlib>
#include <iostream>

using namespace std;

Reader::Reader(const char *s) {
	ifstream f(s, ios::in | ios::binary);
	std::string line;
	if (!f.good()) {
		cerr << "Could not open input file" << endl;
		exit(EXIT_FAILURE);
	}
	getline(f, version);
	if (version == "# vtk DataFile Version 2.0")
		version = "2.0";
	else if (version == "# vtk DataFile Version 3.0")
		version = "3.0";
	else {
		cerr << "Unsupported version `" << version << "'" << endl;
		exit(EXIT_FAILURE);
	}
	getline(f, comment);
	getline(f, line);
	if (line == "ASCII") 
		dataType = ASCII;
	else if (line == "BINARY")
		dataType = BINARY;
	else if (line == "BINGZ")
		dataType = BINGZ;
	else if (line == "BINRGZ")
		dataType = BINRGZ;
	getline(f, line);

	std::string dataset = line.substr(0, 8);
	if (dataset != "DATASET ") {
		cerr << "Expected DATASET, got " << line << endl;
		exit(EXIT_FAILURE);
	}
	try {
		line.erase(0, 8);
		while (isspace(line[0]))
			line.erase(line.begin());
	} catch (...) {
		cerr << "Exception while parsing DATASET field" << endl;
		exit(EXIT_FAILURE);
	}

	if (line == "STRUCTURED_POINTS")
		dataSet = STRUCTURED_POINTS;
	else if (line == "STRUCTURED_GRID")
		dataSet = STRUCTURED_GRID;
	else if (line == "UNSTRUCTURED_GRID")
		dataSet = UNSTRUCTURED_GRID;
	else if (line == "POLYDATA")
		dataSet = POLYDATA;
	else if (line == "RECTILINEAR_GRID")
		dataSet = RECTILINEAR_GRID;
	else if (line == "FIELD")
		dataSet = FIELD;
	else {
		cerr << "Unsupported DATASET type `" << line << "'" << endl;
		exit(EXIT_FAILURE);
	}	
}

ostream &operator<<(ostream &o, const Reader &rd) {
	return o 
		<< "Version: " << rd.version << endl
		<< "Comment: " << rd.comment << endl
		<< "Data type: " << rd.dataTypeString() << endl
		<< "Data set: " << rd.dataSetString() << endl;
}
