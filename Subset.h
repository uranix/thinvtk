#ifndef __SUBSET_H__
#define __SUBSET_H__

#include <ostream>
#include <climits>
#include <cstdlib>

struct Subset {
	unsigned long low, step, high;
	Subset() { }
	Subset(unsigned l, unsigned s, unsigned h) : low(l), step(s), high(h) { }
	bool all(long int np = ULONG_MAX) const {
		return (low == 0) && (step == 1) && ((high == ULONG_MAX) || (high == np - 1));
	}
	unsigned long length() const {
		return (high - low) / step + 1;
	}
	bool parse(const char *s) {
		long int v;
		char *ep, *ep2;

		v = strtol(s, &ep, 10);
		if (v < 0)
			return false;
		low = v;
		if (*ep != ':')
			return false;
		v = strtol(ep + 1, &ep2, 10);
		if (*ep2 != ':')
			return false;
		if (ep2 - ep == 1)
			v = 1;
		if (v <= 0)
			return false;
		step = v;
		v = strtol(ep2 + 1, &ep, 10);
		if (*ep)
			return false;
		if (ep - ep2 == 1)
			v = ULONG_MAX;
		if (v < low)
			return false;
		high = v;
		return true;
	}
	friend std::ostream &operator<<(std::ostream &o, const Subset &ss) {
		o << "from " << ss.low << " to ";
		if (ss.high == ULONG_MAX)
			o << "end";
		else
			o << ss.high;
		return o << " with step " << ss.step;
	}
};

#endif
