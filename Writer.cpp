#include "Writer.h"

#include <cstdlib>
#include <iostream>

using namespace std;

Writer::Writer(const char *output, bool tofloat, int bits, bool group, bool zip, int compression, Subset ss[3]) :
	tofloat(tofloat), group(group), zip(zip),
	bits(bits), compression(compression),
	f(output, ios::out | ios::binary) 
{
	for (int i = 0; i < 3; i++)
		this->ss[i] = ss[i];
	if (!f.good()) {
		cerr << "Could not open output file" << endl;
		exit(EXIT_FAILURE);
	}
}

void Writer::process(const Reader &rd) {
	f << rd;
	f.close();
}
