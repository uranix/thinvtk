#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <cassert> 
#include <vector>
#include <cstdlib>

#include <getopt.h>

#include "Subset.h"
#include "Reader.h"
#include "Writer.h"

#define PROGRAM_NAME "vtkcut"

using namespace std;

void usage() {
	cerr << "Usage: " << PROGRAM_NAME << " [options] <input.vtk> <output.vtk>" << endl
		<< "Options:" << endl
		<< "  --help, -h     \t\tShow this message" << endl
		<< "  --dump, -d     \t\tShow input.vtk info" << endl
		<< "  --to-float, -f\t\tDouble values are converted to float" << endl
		<< "  --bits, -B <n>\t\tZeroes less significant bits after n in mantissa" << endl
		<< "  --extract-i, -i <low:step:high> " << endl
		<< "  --extract-j, -j <low:step:high> " << endl
		<< "  --extract-k, -k <low:step:high> " << endl
		<< "  \t\t\t\tExtracts only subset for index (zero-based numeration)" << endl
		<< "  \t\t\t\t  Ex. 0:5:30 will be [0 5 10 15 20 25 30]" << endl
		<< "  \t\t\t\t  Ex. 2:5:20 will be [2 7 12 17]" << endl
		<< "  \t\t\t\t  Ex. 10::15 will be [10 11 12 13 14 15]" << endl
		<< "  \t\t\t\t  Ex. 10:3: will be [10 13 16 19 22 25 ...]" << endl
		<< "  --gzip=[1-9], -z[1-9]\t\tCompress arrays with gzip *" << endl
		<< "  \t\t\t\t  Optional argument is quality, default is 6" << endl
		<< "  --group-bytes, -g\t\tGroups bytes for better compression *" << endl
		<< "Warinig: * options produce an output in extended VTK format which is " << endl
		<< "  unsupported by current VTK library" << endl
		<< "File always is exported in binary format" << endl
		<< "Option --group-bytes assumes --gzip 6 if other not specified " << endl;
}


int main(int argc, char **argv) {
	int c;
	int index;

	bool dump, binary, tofloat, zip, group, extract;
	int bits = -1, compression = 6;
	dump = binary = tofloat = zip = group = extract = false;

	char *ep;

	Subset ss;
	Subset subsets[3];

	static struct option long_opts[] = {
		{"help", no_argument, 0, 'h'},
		{"dump", no_argument, 0, 'd'},
		{"to-binary", no_argument, 0, 'b'},
		{"to-float", no_argument, 0, 'f'},
		{"bits", required_argument, 0, 'B'},
		{"extract-i", required_argument, 0, 'i'},
		{"extract-j", required_argument, 0, 'j'},
		{"extract-k", required_argument, 0, 'k'},
		{"gzip", optional_argument, 0, 'z'},
		{"group-bytes", no_argument, 0, 'g'},
	};

	while ((c = getopt_long(argc, argv, "hdbfB:i:j:k:z::g", long_opts, &index)) != -1) {
		switch(c) {
			case 'h' : 
				usage();
				return 1;
			case 'd' : 
				dump = true;
				break;
			case 'b' : 
				binary = true;
				break;
			case 'f' : 
				tofloat = true;
				break;
			case 'B' : 
				bits = strtol(optarg, &ep, 10);
				if (*ep || bits <= 0 || bits > 53) {
					cerr << "Bad number `" << optarg << "' for --bits field" << endl;
					return 1;
				}
				break;
			case 'i' : 
			case 'j' : 
			case 'k' : 
				if (!ss.parse(optarg)) {
					cerr << "Failed to parse subset specification `" << optarg << "'" << endl;
					return 1;
				} 
				extract = true;
				subsets[c - 'i'] = ss;
				break;
			case 'z' : 
				zip = true;
				if (optarg) {
					compression = strtol(optarg, &ep, 10);
					if (*ep || compression > 9 || compression < 1) { 
						cerr << "Invalid value `" << optarg << "'specified for gzip compression" << endl;
						return 1;
					}
				}
				break;
			case 'g' : 
				group = true;
				zip = true;
				break;
			case '?' : 
				return 1;
			default: 
				cerr << "getopt returned `" << c << "'. Whatever that means" << endl;
				return 2;
		}
	}

	assert(!group || zip);

	if (tofloat || zip || bits != -1 || extract)
		binary = true;

	if (dump && binary) {
		cerr << "--dump is requested along with another action" << endl;
		return 1;
	}

	if (!dump && !binary) {
		cerr << "Nothing to do" << endl;
		return 1;
	}

	if (dump) {
		if (optind + 1 > argc) {
			cerr << "Input file required for --dump" << endl;
			return 1;
		}
		if (optind + 1 < argc) {
			cerr << "Extra arguments" << endl;
		}
	} else {
		if (optind + 2 > argc) {
			cerr << "Input and output files required" << endl;
			return 1;
		}
		if (optind + 2 < argc) {
			cerr << "Extra arguments" << endl;
		}
	}

	const char *input = argv[optind];
	const char *output = (optind + 1 == argc) ? 0 : argv[optind + 1];

	Reader data(input);

	if (dump) {
		cout << data;
		return 0;
	}

	Writer writer(output, tofloat, bits, group, zip, compression, subsets);
	writer.process(data);

	return 0;
}
